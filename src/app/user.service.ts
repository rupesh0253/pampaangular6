import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

registerNewUser(userdata): Observable <any>{
    return this.http.post('http://0.0.0.0:8000/login/', userdata);
}
getNewUser(userdata): Observable <any>{
  return this.http.get('http://0.0.0.0:8000/getdata/', userdata);
}
}

